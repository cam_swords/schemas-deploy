# Security report schemas changelog

## v2.3.1
- Add unique identifier `id` to vulnerability
- Add `vulnerability.location` to the DAST schema

## v2.3.0
- Initial commit of the security-report-schemas project matching report version outputted by the GitLab Security Common library
