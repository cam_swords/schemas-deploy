# Schemas for GitLab security reports

This repository defines the schemas for the security reports emitted by GitLab security scanners. It defines the reports for:
- [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/)

The schemas are defined using [JSON Schema](https://json-schema.org/). Any security scanner that integrates into GitLab must produce a JSON report that adheres to one of above schemas.

## How to modify a schema
Schemas are made from a combination of the base `security-report-format.json` schema and an extension schema that belongs to a report type (e.g. `sast-report-format.json`). These files can be found in the `src` directory of the project.

### Determining where the change belongs 
When adding new fields to a schema, please add it to the appropriate place:

- If a field is experimental, it likely belongs in the specific report schema
- If a field is used by few report types, it likely belongs in each report type schema
- If a field is to be used by all report types, it likely belongs in the base schema

### Adding a new field to an extension schema

New fields in extension schemas must be added to the `allOf` array in the source schema file. 
For example, if DAST was to extend the vulnerability definition, and add a new required field called `evidence`:

```json
 "allOf": [
    { "$ref": "security-report-format.json" },
    {
      "properties": {
        "vulnerabilities": {
          "items": {
            "properties": { "evidence": { "type": "string" } },
            "required": [ "evidence" ]
          }
        }
      }
    }
  ]
```

Adding the new field to the normal `properties` field in the extension schema, or redefining `definitions` with the new field definition will not result in the field being contained in the generated schema output.

When adding a new field, please add an appropriate check in the `tests/test-*.sh` file to verify that the merge contains the field you expect.

### Pre-commit expectations

Security Report schemas are released using Git tags. To ensure that a successful run of a CI Pipeline on the `master` branch of the project can immediately be released,
engineers are required to check in the generated `dist` output whenever they make a change to the schema.

The `dist` output can be generated by running `scripts/distribute.sh`, as described below. Tests in the CI Pipeline will ensure this practice is followed.
       

### Limitations
Please do not remove fields, rename fields, or change the type of fields without knowing what you're doing. These are breaking changes, and will require a major breaking release.

## How to release a new version of the schema

Schemas are released by unifying the base schema with each report schema. All the `$ref` and `allOf` references are inlined, duplicate fields are merged, and the output is written to the `dist` directory. This is done by engineers with the help of script files.

Prior to release:
- Install `jq` and `npm`.
- Run `npm install bin/merge`.

### Instructions
- Check out the schema repository and create a new branch
- Update `self.version` in `src/security-report-format.json` to the new version
- Ensure the `CHANGELOG.md` is up to date and accurately describes the changes and the version corresponds to `self.version` above
- Run `scripts/distribute.sh`
- Commit and push the new changes (both `src` and `dist` directories)
- Create a Merge Request; after the new branch pipeline succeeds and you get approval, you can merge changes into `master`
- Tag the newly merged commit (and add relase comments to create a release) with the version indicated in the CHANGELOG and `schema-version`
- Communicate to the `#s_secure` Slack channel that the release has been made

## Versioning schemas

This repository follows the SchemaVer standard for versioning json schemas. Please read more about it [here](https://github.com/snowplow/iglu/wiki/SchemaVer).

## Testing

Tests are used to verify that the appropriate fields are present in the Secure schemas. 
The tests are implemented using the [`bash_unit`](https://github.com/pgrange/bash_unit) framework.

To run the tests, follow these steps:

1. [Install `bash_unit`](https://github.com/pgrange/bash_unit#other-installation) on Mac OS:

    ```bash
    bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)
    mv ./bash_unit /usr/local/bin
    ```

1. Install [`jq`](https://stedolan.github.io/jq/) on Mac OS:

    ```bash
    brew install jq
    ```

1. Install the required npm modules

    ```bash
    npm install bin/merge
    ```

1. Run the tests

    ```bash
    bash_unit ./test/test.sh
    ```

## Contributing
If you want to help and extend the list of supported scanners, read the
contribution guidelines https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/CONTRIBUTING.md
