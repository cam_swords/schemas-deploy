#!/usr/bin/env bash

set -e

error() {
  printf "$*" >>/dev/stderr
  exit 1
}

PROJECT_DIR="$(dirname "$(realpath "$0")")/.."
CHANGELOG_FILE=$(realpath "$PROJECT_DIR/CHANGELOG.md")
VERSION_PATTERN='^## \(v[0-9]*\.[0-9]*\.[0-9]*\)$'


# changelog_last_version checks and returns the version
# of the most recent changelog entry (first to appear in the file).
# It fails if the version is not Semver-compliant or is a pre-release.
changelog_last_version() {
  if ! [ -f "$CHANGELOG_FILE" ]; then
    error "Unable to find $CHANGELOG_FILE."
  fi

  # find the first matching version, e.g. ## v1.6.0
  VERSION=$(sed -n "s/$VERSION_PATTERN/\\1/p" "$CHANGELOG_FILE" | sed -n '1,1p;1q')

  if [ -z "$VERSION" ]; then
    error "Aborting, unable to determine the latest version in the changelog file. Expected line with version to have format like '## v1.3.6'."
  fi

  # find the first line that starts with ## (should also be the first matching version)
  MOST_RECENT_VERSION=$(grep -m 1 '^##.*$' "$CHANGELOG_FILE" | sed 's/## //')

  if [ "$MOST_RECENT_VERSION" != "$VERSION" ]; then
    error "The most recent version in the changelog $MOST_RECENT_VERSION does not conform to the expected format. Expected version line to have format like '## v1.3.6'."
  fi

  # mark versions as Release Candidate until the Secure Report Format is ready for General Availability.
  echo "$VERSION-rc1"
}


# changelog_last_description checks and returns the description
# of the most recent changelog entry (first to appear in the file).
# It fails if the version is not Semver-compliant or is a pre-release.
changelog_last_description() {
  if ! [ -f "$CHANGELOG_FILE" ]; then
    error "Unable to find $CHANGELOG_FILE."
  fi

  # extract the latest version description from the CHANGELOG
  CHANGELOG_DESCRIPTION_START=$(sed -n "/$VERSION_PATTERN/=" "$CHANGELOG_FILE" | sed -n '1,1p;1q' | awk '{print $0 + 1}')
  CHANGELOG_DESCRIPTION_END=$(sed -n "/$VERSION_PATTERN/=" "$CHANGELOG_FILE" | sed -n '2,2p;2q' | awk '{print $0 - 2}')
  CHANGELOG_DESCRIPTION=$(sed -n "${CHANGELOG_DESCRIPTION_START},${CHANGELOG_DESCRIPTION_END}p;${CHANGELOG_DESCRIPTION_END}q" "$CHANGELOG_FILE")

  echo "$CHANGELOG_DESCRIPTION"
}
