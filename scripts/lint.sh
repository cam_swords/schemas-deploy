#!/usr/bin/env bash

set -e

os=""
case $(uname -a) in
  Linux*) os="linux";;
  Darwin*) os="mac";;
  *) echo "unsupported os"; exit 1;;
esac

for i in `find src -type f`; do 
  echo -n "$i: "
  cat $i | bin/jsonlint-$os
done
