#!/usr/bin/env bash

set -e

error() {
  printf "$*" >>/dev/stderr
  exit 1
}


# build_release_json_payload builds a payload that can be used to create a GitLab release via the API.
# arguments: [Version] [Changelog description] [Project URL]
# Line lines are stripped from the changelog, otherwise the GitLab release will be malformatted.
build_release_json_payload() {
  if [ -z "$1" ]; then
    error "Aborting, version has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$2" ]; then
    error "Aborting, changelog description has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$3" ]; then
    error "Aborting, Project URL has not been supplied to ${FUNCNAME[0]}"
  fi

  DESCRIPTION=""
  DESCRIPTION="$DESCRIPTION ##### Changes\n"
  DESCRIPTION="$DESCRIPTION $2"
  DESCRIPTION="$DESCRIPTION \n\n"
  DESCRIPTION="$DESCRIPTION ##### Secure Report Format Schemas\n"
  DESCRIPTION="$DESCRIPTION - $3/-/raw/$1/dist/container-scanning-report-format.json\n"
  DESCRIPTION="$DESCRIPTION - $3/-/raw/$1/dist/dast-report-format.json\n"
  DESCRIPTION="$DESCRIPTION - $3/-/raw/$1/dist/sast-report-format.json\n"
  DESCRIPTION="$DESCRIPTION - $3/-/raw/$1/dist/dependency-scanning-report-format.json\n"

  UNSAFE_RELEASE_DATA="{\"tag_name\":\"$1\",\"description\":\"$DESCRIPTION\"}"

  # use node to help replace new lines with \n
  RELEASE_DATA=$(echo "$UNSAFE_RELEASE_DATA" | \
                 node -e "console.log((require('fs')).readFileSync(process.stdin.fd, 'utf-8').replace('\n', '\\\n'));")

  JSON_TYPE=$(echo "$RELEASE_DATA" | jq type | sed "s/\"//g")

  if [ "$JSON_TYPE" != "object" ]; then
    error "Aborting, extracted release data type '$JSON_TYPE' is not a JSON object $RELEASE_DATA"
  fi

  EXTRACTED_TAG_NAME=$(echo "$RELEASE_DATA" | jq ".tag_name" | sed "s/\"//g")
  EXTRACTED_DESCRIPTION=$(echo "$RELEASE_DATA" | jq ".description" | sed "s/\"//g")

  if [ -z "$EXTRACTED_TAG_NAME" ]; then
    error "Aborting, unable to determine the tag name from the release data $RELEASE_DATA"
  fi

  if [ -z "$EXTRACTED_DESCRIPTION" ]; then
    error "Aborting, unable to determine the description from the release data $RELEASE_DATA"
  fi

  echo "$RELEASE_DATA"
}


# verify_version_not_released ensures that there is not already a release for the version attempting to be released.
# arguments: [GitLab API token] [CI project ID] [Version]
verify_version_not_released() {
  if [ -z "$1" ]; then
    error "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$2" ]; then
    error "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$3" ]; then
    error "Aborting, version has not been supplied to ${FUNCNAME[0]}"
  fi

  VERSION_URL="https://gitlab.com/api/v4/projects/$2/repository/tags/$3"

  if curl --silent --fail --show-error --header "private-token:$1" "$VERSION_URL"; then
    error "\nAborting, tag $3 already exists. If this is not expected, please remove the tag and try again.\n"
  fi
}


# tag_git_commit uses the GitLab API to create a lightweight Git tag.
# arguments: [GitLab API token] [CI Project ID] [Tag name] [Commit SHA]
tag_git_commit() {
  if [ -z "$1" ]; then
    error "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$2" ]; then
    error "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$3" ]; then
    error "Aborting, tag name has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$4" ]; then
    error "Aborting, commit SHA has not been supplied to ${FUNCNAME[0]}"
  fi

  TAG_URL="https://gitlab.com/api/v4/projects/$2/repository/tags?tag_name=$3&ref=$4"

  curl --silent --fail --show-error --request POST --header "PRIVATE-TOKEN:$1" "$TAG_URL"
}


# create_gitlab_release uses the GitLab API to create a GitLab release.
# The Git tag should be created before this function is run.
# arguments: [GitLab API token] [CI Project ID] [Release payload]
create_gitlab_release() {
  if [ -z "$1" ]; then
    error "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$2" ]; then
    error "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  fi

  if [ -z "$3" ]; then
    error "Aborting, release payload has not been supplied to ${FUNCNAME[0]}"
  fi

  RELEASE_URL="https://gitlab.com/api/v4/projects/$2/releases"

  echo "curl --silent --fail --show-error --request POST --header 'PRIVATE-TOKEN:$1' --header 'Content-Type:application/json' --data '$3' '$RELEASE_URL'"
  curl --silent --fail --show-error --request POST \
       --header "PRIVATE-TOKEN:$1" \
       --header 'Content-Type:application/json' \
       --data "$3" \
       "$RELEASE_URL"
}







