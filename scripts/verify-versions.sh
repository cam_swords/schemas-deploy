#!/bin/bash

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/.."
CHANGELOG_FILE=$(realpath "$PROJECT_DIR/CHANGELOG.md")
SECURE_REPORT_FORMAT_SRC=$(realpath "$PROJECT_DIR/src/security-report-format.json")
SECURE_REPORT_FORMAT_DAST_DIST=$(realpath "$PROJECT_DIR/dist/dast-report-format.json")
PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}

error() {
  echo "$*" >>/dev/stderr
  exit 1
}

# changelog_last_version checks and returns the version
# of the most recent changelog entry (first to appear in the file).
# It fails if the version is not Semver-compliant or is a pre-release.
changelog_last_version() {
  if ! [ -f "$CHANGELOG_FILE" ]; then
    error "Unable to find $CHANGELOG_FILE."
  fi

  # find the first matching version, e.g. ## v1.6.0
  VERSION_PATTERN='^## \(v[0-9]*\.[0-9]*\.[0-9]*\)$'
  VERSION=$(sed -n "s/$VERSION_PATTERN/\\1/p" "$CHANGELOG_FILE" | sed -n '1,1p;1q')

  if [ -z "$VERSION" ]; then
    error "Aborting, unable to determine the latest version in the changelog file. Expected line with version to have format like '## v1.3.6'."
  fi

  # find the first line that starts with ## (should also be the first matching version)
  MOST_RECENT_VERSION=$(grep -m 1 '^##.*$' "$CHANGELOG_FILE" | sed 's/## //')

  if [ "$MOST_RECENT_VERSION" != "$VERSION" ]; then
    error "The most recent version in the changelog $MOST_RECENT_VERSION does not conform to the expected format. Expected version line to have format like '## v1.3.6'."
  fi

  echo "$VERSION"
}

# report_schema_version returns the version of a JSON report schema.
report_schema_version() {
  SCHEMA_FILE="$1"

  if ! [ -f "$SCHEMA_FILE" ]; then
    error "Unable to find $SCHEMA_FILE."
  fi

  VERSION=$(jq -r -e ".self.version" "$SCHEMA_FILE")

  if [ -z "$VERSION" ]; then
    error "Aborting, unable to determine the latest self.version in $SCHEMA_FILE."
  fi

  echo "v$VERSION"
}

# compare_schema_content_with_dist compares the content of a JSON schema
# with the content of its distribution, and fails if they are different.
compare_schema_content_with_dist() {
    VERSION="$1"
    REPORT_TYPE="$2"
    TEMP_FILE="$TMPDIR/$REPORT_TYPE.schema.json"

    # When schemas are mature enough to be released to a wider audience, we can remove the release candidate suffix
    curl --silent --fail --output "$TEMP_FILE" "$PROJECT_URL/-/raw/$VERSION-rc1/dist/$REPORT_TYPE-report-format.json"

    # temporarily disable error handling, so we can display a better error message
    set +e
    diff -u "$TEMP_FILE" "dist/$REPORT_TYPE-report-format.json"
    COMPARISON_RESULT="$?"
    set -e

    if [ "$COMPARISON_RESULT" != "0" ]; then
      echo
      error "The released version of the $REPORT_TYPE schema differs from what is in the dist directory, yet" \
            "the versions are the same." \
            "Did you change the schema, and forget to increment the version?"
    fi
}


VERSION_ALREADY_RELEASED=false
CHANGELOG_VERSION=$(changelog_last_version)
SRC_VERSION=$(report_schema_version "$SECURE_REPORT_FORMAT_SRC")

# All schemas ready for distribution will have the same version, DAST is used as an example to represent all.
DAST_DIST_VERSION=$(report_schema_version "$SECURE_REPORT_FORMAT_DAST_DIST")

# If curl returns a 404, we know the version has not been released.
# When schemas are mature enough to be released to a wider audience, we can remove the release candidate suffix
if curl -X HEAD --silent --fail --output /dev/null "$PROJECT_URL/-/releases/$CHANGELOG_VERSION-rc1"; then
  VERSION_ALREADY_RELEASED=true
fi

echo "Version found in CHANGELOG:          $CHANGELOG_VERSION"
echo "Version found in src/:               $SRC_VERSION"
echo "Version found in dist/:              $DAST_DIST_VERSION"
echo "Changelog version already released:  $VERSION_ALREADY_RELEASED"
echo

if [ "$CHANGELOG_VERSION" != "$SRC_VERSION" ]; then
  error "Aborting, the version in the CHANGELOG and the source JSON have diverged." \
        "Do you need to add a CHANGELOG entry, or did you forget to update self.version?"
fi

if [ "$SRC_VERSION" != "$DAST_DIST_VERSION" ]; then
  error "Aborting, the version in the source JSON and the version in the distributed JSON have diverged." \
        "Please make sure self.version is up-to-date."
fi

if [ "$VERSION_ALREADY_RELEASED" == "true" ]; then
  compare_schema_content_with_dist "$SRC_VERSION" "container-scanning"
  compare_schema_content_with_dist "$SRC_VERSION" "dast"
  compare_schema_content_with_dist "$SRC_VERSION" "sast"
  compare_schema_content_with_dist "$SRC_VERSION" "dependency-scanning"
fi

echo "Nice work! All checks have passed."
