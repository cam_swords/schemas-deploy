#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
DS_SCHEMA="$PROJECT_DIRECTORY/dist/dependency-scanning-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_dependency_scanning_contains_common_definitions() {
  verify_schema_contains_selector "$DS_SCHEMA" ".title"
  verify_schema_contains_selector "$DS_SCHEMA" ".description"
  verify_schema_contains_selector "$DS_SCHEMA" ".self.version"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.schema"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.version"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.category"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.name"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.message"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.description"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.cve"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.severity"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.confidence"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.solution"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.scanner"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.identifiers"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.links"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.remediations.items.properties.fixes"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.remediations.items.properties.summary"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.remediations.items.properties.diff"
}

test_should_not_contain_definitions() {
  verify_schema_doesnt_contain_selector "$DS_SCHEMA" ".definitions"
}

test_dependency_scanning_extensions() {
  verify_schema_contains_selector "$DS_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency"

  verify_schema_contains_selector "$DS_SCHEMA" 'select(.required[] | contains("dependency_files"))'
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.path"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.package_manager"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.package.properties.name"
  verify_schema_contains_selector "$DS_SCHEMA" ".properties.dependency_files.items.properties.dependencies.items.properties.version"
}
