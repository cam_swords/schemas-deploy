#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SAST_SCHEMA="$PROJECT_DIRECTORY/dist/sast-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_sast_contains_common_definitions() {
  verify_schema_contains_selector "$SAST_SCHEMA" ".title"
  verify_schema_contains_selector "$SAST_SCHEMA" ".description"
  verify_schema_contains_selector "$SAST_SCHEMA" ".self.version"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.schema"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.version"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.category"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.name"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.message"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.description"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.cve"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.severity"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.confidence"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.solution"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.scanner"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.identifiers"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.links"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.remediations.items.properties.fixes"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.remediations.items.properties.summary"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.remediations.items.properties.diff"
}

test_should_not_contain_definitions() {
  verify_schema_doesnt_contain_selector "$SAST_SCHEMA" ".definitions"
}

test_sast_extensions() {
  verify_schema_contains_selector "$SAST_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.start_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.end_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.class"
}
